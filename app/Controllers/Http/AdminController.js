'use strict'
const Order = use('App/Models/Order')
const User = use('App/Models/User')
const Product = use('App/Models/Product')
const { validate } = use('Validator')

class AdminController {

    async index({request, view, auth, response}){

        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if(!auth.user.is_admin){
            return response.redirect('back')
        }

        var total_orders = await Order.query().getCount()
        var total_products = await Product.query().getCount()
        var total_users = await User.query().where('is_admin','=',0).getCount()


        return view.render('admin.index', {
            total_orders: total_orders,
            total_products: total_products,
            total_users: total_users,
        });
    }

    async doLogin({request, session, auth, response}){
        const { email, password } = request.all()

        var rules = {
            email: 'required|email',
            password: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {

            session
                .withErrors(validation.messages())

            return response.redirect('back')
        }

        var user = await User.query().where('email', '=', email).first();

        if (!user.is_admin) {
            return response.redirect('back');
        }

        try {
            var logged_in = await auth.attempt(email, password);

        } catch (e) {
            session
                .withErrors(validation.messages())
            return response.redirect('back');
        }


        return response.redirect('/admin')
    }
}

module.exports = AdminController
