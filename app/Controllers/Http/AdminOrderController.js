'use strict'
const Order = use('App/Models/Order')
const Product = use('App/Models/Product')

class AdminOrderController {

    async index({ request, view, auth, response }) {

        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }
        var products = await Product.query().whereNull('deleted_at').fetch()

        var product_ids = await this.extractObjectValueByKeys(products, 'id');

        var orders = await Order.query().whereIn('product_id',product_ids).with('product').with('user').fetch();

        return view.render('admin.order.index', { orders: orders.toJSON() });
    }

    async show({request, auth, params, view, response}){
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        var order = await Order.query().where('id','=',params.id).with('product').with('user').first();

        return view.render('admin.order.show', { order: order.toJSON() });
    }

    async delete({ request, auth, view, response }) {
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        const { order_id } = request.all()

        var update = await Order.query().where('id', '=', order_id).delete();

        return response.redirect('back');

    }

    

    async update({request, auth, view, response}){
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        const { order_id, status } = request.all()

        var update = await Order.query().where('id', '=', order_id).update({status: status})

        return response.redirect('back');

    }

    async extractObjectValueByKeys(arrayObj, key_name) {
        let arr = [];
        
        arrayObj.toJSON().map((v, k) => { arr.push(v[key_name]) })

        return arr;
    }
}

module.exports = AdminOrderController
