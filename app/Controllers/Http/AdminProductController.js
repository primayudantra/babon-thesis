'use strict'
const Product = use('App/Models/Product')

class AdminProductController {
    async index({ request, view, auth, response }) {

        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        var products = await Product.query().whereNull('deleted_at').fetch();

        return view.render('admin.product.index', { products: products.toJSON() });
    }

    async update({ request, auth, params, view, response }) {
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        var product = await Product.query().where('id', '=', params.id).first();

        return view.render('admin.product.show', { product: product.toJSON() });
    }

    async create({ request, auth, params, view, response }) {
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        return view.render('admin.product.create');
    }

    async store({ request, auth, view, response }) {
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        let {name, price, description} = request.all();

        let product = new Product();

        product.name = name;
        product.price = price;
        product.description = description;

        await product.save()

        return response.redirect('/admin/products')
    }

    async doUpdate({ request, auth, view, response }) {
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        let { product_id, name, price, description } = request.all();

        var update = await Product.query().where('id', '=', product_id).update({ name: name, price: price, description: description });

        return response.redirect('/admin/products');

    }

    async delete({ request, auth, response }) {
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        let { product_id } = request.all();

        var delete_product = await Product.query().where('id', '=', product_id).update({ deleted_at: new Date });

        return response.redirect('/admin/products');

    }
}

module.exports = AdminProductController
