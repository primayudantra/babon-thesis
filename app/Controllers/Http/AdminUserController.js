'use strict'
const User = use('App/Models/User')

class AdminUserController {
    async index({request, auth,view, response}){
        try {
            await auth.check()
        } catch (error) {
            return response.route('login')
        }

        if (!auth.user.is_admin) {
            return response.redirect('back')
        }

        var users = await User.query().where('is_admin','=', 0).fetch();

        return view.render('admin.user.index', { users: users.toJSON() });
    }
}

module.exports = AdminUserController
