'use strict'

const User = use('App/Models/User')
const { validate } = use('Validator')


class AuthController {

	login ({view, auth}) { 
		return view.render('login')
  	}

  	async logout ({auth, response}) { 

		await auth.logout()

		return response.redirect('/')

  	}

  	async doLogin ({request, session, auth, response}) { 
		const { email, password } = request.all()

  		var rules = {
		      email: 'required|email',
		      password: 'required'
		}

		const validation = await validate(request.all(), rules)

		if (validation.fails()) {

		      session
		        .withErrors(validation.messages())

	      return response.redirect('back')
    	}

    	try{
    		var logged_in = await auth.attempt(email, password);

    	}catch(e){
    		session
		        .withErrors(validation.messages())
    		return response.redirect('back');
    	}
		

    	return response.redirect('/')
  	}

  	register({view}){
  		return view.render('register');
  	}

   	async doRegister({request, session, auth, response}) { 

		const data = request.only(['username', 'email', 'first_name','last_name','password', 'password_confirmation'])

		var rules = {
		      username: 'required|unique:users',
		      email: 'required|email',
		      password: 'required',
		      first_name: 'required',
		      last_name: 'required',
		      password_confirmation: 'required_if:password|same:password',
		}

		const validation = await validate(data, rules)
		if (validation.fails()) {
		      session
		        .withErrors(validation.messages())
				.flashExcept()

				console.log(validation.messages());

	      return response.redirect('back');
    	}

	    delete data.password_confirmation

    	const user = await User.create(data)

		return response.redirect('/login')
   	}

}

module.exports = AuthController
