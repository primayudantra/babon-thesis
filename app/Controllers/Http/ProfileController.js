'use strict'
const Product = use('App/Models/Product')
const Order = use('App/Models/Order')

class ProfileController {

	async show({request, response, auth, view}){

		try {
			await auth.check()
		} catch (error) {
			return response.route('login')
		}
		
		var user = auth.user;

		var orders = await Order.query().where('user_id','=', user.id).with('product').fetch();

		orders = orders.toJSON();

		return view.render('profile', {user: user, orders: orders})
	}

	async order({request, response, auth, view}){

		var user = auth.user;
		
		var products = await Product.query().whereNull('deleted_at').fetch()

		products = products.toJSON()

		return view.render('order', {user, products})

	}

	async doOrder({request, response, auth, view}){

		var user = auth.user;

		const { messages, product_id } = request.all()

		let order = new Order();

		order.user_id = user.id;
		order.product_id = product_id;
		order.messages = messages;
		order.status = 'Pending';

		await order.save()

		return response.redirect('/profile')

	}

}

module.exports = ProfileController
