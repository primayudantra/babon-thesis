'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnInUsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('first_name', 255).notNullable()
      table.string('last_name', 255).notNullable()
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddColumnInUsersSchema
