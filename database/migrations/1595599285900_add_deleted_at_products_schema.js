'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddDeletedAtProductsSchema extends Schema {
  up () {
    this.table('products', (table) => {
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.table('products', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddDeletedAtProductsSchema
