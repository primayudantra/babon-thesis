'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('index')

// Login
Route.get('/login', 'AuthController.login')
Route.post('/do-login', 'AuthController.doLogin')

// Logout
Route.get('/logout', 'AuthController.logout')

// Register
Route.get('/register','AuthController.register')
Route.post('/do-register','AuthController.doRegister');

Route.get('/profile','ProfileController.show').middleware('isAuth')
Route.get('/order','ProfileController.order').middleware('isAuth')
Route.post('/do-order','ProfileController.doOrder').middleware('isAuth')

// Admin Section
Route.group(() => {

  Route.get('', 'AdminController.index').middleware('isAuth');
  Route.on('login').render('admin.login')
  Route.post('/do-login', 'AdminController.doLogin')

  Route.get('/users', 'AdminUserController.index')

  // Order
  Route.get('/orders', 'AdminOrderController.index')
  Route.get('/order/:id', 'AdminOrderController.show')
  Route.post('/delete-order', 'AdminOrderController.delete').as('admin.delete_order')
  Route.post('/update-order', 'AdminOrderController.update')

  // Product
  Route.get('/products', 'AdminProductController.index')
  Route.get('/products/create', 'AdminProductController.create')
  Route.get('/products/:id', 'AdminProductController.update')
  Route.post('/do-update-product', 'AdminProductController.doUpdate')

  Route.post('/do-store-product', 'AdminProductController.store')
  Route.post('/do-delete-product', 'AdminProductController.delete')
  
  

}).prefix('admin')